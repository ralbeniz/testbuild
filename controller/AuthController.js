const io = require('../io');
const crypt = require('../crypt');
const requestJson = require('request-json');
const baseMLabURL = "https://api.mlab.com/api/1/databases/apitechurpg6ed/collections/";
const mLabAPIKey = "apiKey=HBuLThbtN6200gFNWTUaLnmxHe6Ap7GB";

function loginV1(req,res) {
  console.log("POST /apitechu/v1/loging");
  console.log("email es " + req.body.email);
  console.log("PW es "    + req.body.password);
  var users = require('../usuarios.json');
  var finded = false;

  for (user of users){
    console.log(user.password);
    console.log(req.body.password);
    if ((user != null && user.email == req.body.email) && (user.password == req.body.password)){
      console.log("entro en el IF222");
          finded = true;
          user.logged = true;
          res.send("Loging correcto");
          io.writeUserDataFile(users);
        }
  }
  if (finded != true){
    res.send("Loging incorrecto");
}
};




function logoutV1(req,res) {
  console.log("POST /apitechu/v1/logout");
  console.log("id recibido " + req.body.id);
  var users = require('../usuarios.json');
  var finded = false;
  for (user of users){
    console.log("objeto; " + user);
    console.log("posicion del array " + user.id);
    if ((user != null && user.id == req.body.id) && (user.logged == true)){
      console.log("entro en el IF");
      finded = true;
      delete user.logged;
      res.send("Logout correcto");
    }
  }
  if (finded != true){
    res.send("Logout incorrecto");
  }
}


function loginV2(req,res) {
  console.log("POST /apitechu/v1/loginV2");
  console.log("email es " + req.body.email);

  var email = req.body.email;
  var query = 'q={"email": "' + email + '"}';

  var httpClient = requestJson.createClient(baseMLabURL);
  console.log("Cliente created");
  httpClient.get("user?" + query + "&" + mLabAPIKey,
     function(err, resMLab, body){
       if (err) {
           response = {
               "msg" : "Error obteniendo usuario"
            }
            res.status(500);
          } else {
             if (body.length > 0) {
                var userMongo = body[0];
                console.log(userMongo.email);
                console.log(userMongo.password);
                if ((userMongo != null && userMongo.email == req.body.email) && (userMongo.password == req.body.password)){
                  quey = 'q={"id" : ' + body[0].id +'}';
                  var putBody = '{"$set":{"logged":true}}';
                  httpClient.put("user?" + query + "&" + mLabAPIKey, JSON.parse(putBody));
                  res.send("Loging correcto");
                } else {
                  res.send("Loging incorrecto");
                }
             } else {
                 var response = {
                   "msg" : "Usuario no encontrado"
                     }
                 res.status(404);
               }
             }
       res.send(response);
       }
     );
   }

function logoutV2(req,res) {
     console.log("POST /apitechu/v1/logoutV2");
     var id = req.params.id;
     var query = 'q={"id":' + id + '}';

     var httpClient = requestJson.createClient(baseMLabURL);
     httpClient.get("user?" + query + "&" + mLabAPIKey,
     function(err, resMLab, body){
        if (err) {
            response = {
                "msg" : "Error obteniendo usuario"
             }
             res.status(500);
           } else {
              if (body.length > 0) {
                 var userMongo = body[0];
                 console.log("req.params.id:" + req.params.id);
                 console.log("UserMongo.id:" + userMongo.id);
                 console.log("UserMongo.logged:" + userMongo.logged);
                 if ((userMongo != null && userMongo.id == req.params.id) && (userMongo.logged == true)){
                   var putBody = '{"$unset":{"logged":""}}';
                   httpClient.put("user?" + query + "&" + mLabAPIKey, JSON.parse(putBody));
                   res.send("Logout correcto");
                 } else {
                   res.send("El usuario no está logado");
                 }
              } else {
                  var response = {
                    "msg" : "Usuario no encontrado"
                      }
                  res.status(404);
                }
              }
                  //var response = !err ? body : {
                  //"msg" : "Error obteniendo usuario."
                  //}
            res.send(response);
            }
          );
        }

module.exports.loginV1 = loginV1;
module.exports.logoutV1 = logoutV1;
module.exports.loginV2 = loginV2;
module.exports.logoutV2 = logoutV2;
