const io = require('../io');
const crypt = require('../crypt');
const requestJson = require('request-json');
const baseMLabURL = "https://api.mlab.com/api/1/databases/apitechurpg6ed/collections/";
const mLabAPIKey = "apiKey=HBuLThbtN6200gFNWTUaLnmxHe6Ap7GB";



function getUsersV1(req, res) {
       console.log("GET /apitechu/v1/users");
       console.log(req.query);

       var result = {};
       var users = require('../usuarios.json');

       if (req.query.$count == "true") {
         console.log("Count needed");
         result.count = users.length;
       }

       result.users = req.query.$top ?
       users.slice(0, req.query.$top) : users;

       res.send(result);

       //dirname indica que es el directorio donde estoy
   //    res.sendFile('usuarios.json', {root: __dirname});

       //var users = require('./usuarios.json');
       //res.send(users);
     }



function CreateUserV1(req,res) {
  console.log("first_name es " + req.body.first_name);
  console.log("last_name es " + req.body.last_name);
  console.log("email es " + req.body.email);

  var newUser = {
    "first_name" : req.body.first_name,
    "last_name" : req.body.last_name,
    "email" : req.body.email,
  };

  var users = require('../usuarios.json');
  users.push(newUser);
  io.writeUserDataFile(users);
  console.log("Usuario añadido con éxito");
  //TRAMPA PARA VER SI SE HA CARGADO EL USUARIO, PERO ESTO NO SE HACE...
  res.send({"msg" : "Usuario añadido con éxito"});
}


function deleteUserV1 (req,res) {
  console.log("DELETE /apitechu/v1/users/:id");
  console.log("id es " + req.params.id);

  var users = require('../usuarios.json');
//    users.splice(req.params.id - 1, 1);
  var idrecibida = req.params.id;
  var index = 0;

  for (user of users){
    console.log("objeto; " + user);
    console.log("idobjeto" + user.id);
    console,log("Length of array is " + users.length);
    if (user != null && user.id == idrecibida){
      delete users[index];
      io.writeUserDataFile(users);
      console.log("Usuario borrado");
      res.send({"msg" : "Usuario borrado"});
      break;
    }else{
      index++;
      console.log("idfichero; " + index);
    }
  }
}

function getUsersV2(req, res) {
       console.log("GET /apitechu/v2/users");

       var httpClient = requestJson.createClient(baseMLabURL);
       console.log("Cliente created");
       httpClient.get("user?" + mLabAPIKey,
          function(err, resMLab, body){
            var response = !err ? body : {
            "msg" : "Error obteniendo usuarios."
            }
            res.send(response);
            }
          );
        }

function getUserbyIdV2(req, res) {
       console.log("GET /apitechu/v2/users/:id");

       var id = req.params.id;
       var query = 'q={"id":' + id + '}';

       var httpClient = requestJson.createClient(baseMLabURL);
       console.log("Cliente created");
       httpClient.get("user?" + query + "&" + mLabAPIKey,
      function(err, resMLab, body){
          if (err) {
              response = {
                  "msg" : "Error obteniendo usuario"
               }
               res.status(500);
             } else {
                if (body.length > 0) {
                   var response = body[0];
                } else {
                    var response = {
                      "msg" : "Usuario no encontrado"
                        }
                    res.status(404);
                  }
                }
                    //var response = !err ? body : {
                    //"msg" : "Error obteniendo usuario."
                    //}
              res.send(response);
              }
            );
          }

function CreateUserV2(req,res) {
   console.log("/apitechu/v2/users" + req.body.first_name);
   console.log("first_name es " + req.body.first_name);
   console.log("last_name es " + req.body.last_name);
   console.log("email es " + req.body.email);
   console.log("id es " + req.body.id);

   var newUser = {
     "id" : req.body.id,
     "first_name" : req.body.first_name,
     "last_name" : req.body.last_name,
     "email" : req.body.email,
     "password" : crypt.hash(req.body.password)
   }
   var httpClient = requestJson.createClient(baseMLabURL);
     console.log("Cliente created");
     httpClient.post("user?" + mLabAPIKey, newUser,
    function(err, resMLab, body){
      console.log("Usuario creado con exito");
      res.send({"msg" : "Usuario creado con éxito"})
   }
 );
}

module.exports.getUsersV1 = getUsersV1;
module.exports.CreateUserV1 = CreateUserV1;
module.exports.deleteUserV1 = deleteUserV1;
module.exports.getUsersV2 = getUsersV2;
module.exports.getUserbyIdV2 = getUserbyIdV2;
module.exports.CreateUserV2 = CreateUserV2;
