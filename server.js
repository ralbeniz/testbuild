// el require es para traerme la funcion express que está dentro de la carpeta node_modules,
// aunque lo he instalado, necesito hacer esto para usarlo
const express = require('express');
const app = express();
const port = process.env.PORT || 3000;

const bodyParser = require('body-parser');

app.use(bodyParser.json());

app.listen(port);
console.log("api cambio escuchando el puerto " + port);


const userController = require('./controller/UserController');
const authController = require('./controller/AuthController');


app.get('/apitechu/v1/hello',
//funcion request and response
  function (req,res) {
    console.log("GET /apitechu/v1/hello");

    //res es la respuesta
    res.send({"msg" :"Holaaaaaaaa desde ApiTechU!!!"});
  }
);

app.get('/apitechu/v1/users', userController.getUsersV1);

app.get('/apitechu/v1/users2',
function(req, res) {
       console.log("GET /apitechu/v1/users2");
       var users2 = require('./usuarios2.json');
       res.send(users2);
     }
);




app.post('/apitechu/v1/users',userController.CreateUserV1);


// el :id es un parametro
app.delete('/apitechu/v1/users/:id',userController.deleteUserV1);



app.post('/apitechu/v1/monstruo/:p1/:p2',
  function(req,res){
    console.log("POST /apitechu/v1/monstruo/:p1/:p2");

    console.log("Parametros");
    console.log(req.params);

    console.log("Query Sring");
    console.log(req.query);

    console.log("Headers");
    console.log(req.headers);

    console.log("Body");
    console.log(req.body);
  }
);


app.post('/apitechu/v1/loging',authController.loginV1);

app.post('/apitechu/v1/logout',authController.logoutV1);

app.get('/apitechu/v2/users',userController.getUsersV2);

app.get('/apitechu/v2/users/:id',userController.getUserbyIdV2);

app.post('/apitechu/v2/users',userController.CreateUserV2);

app.post('/apitechu/v1/loginV2',authController.loginV2);

app.post('/apitechu/v2/logoutV2/:id',authController.logoutV2);
